//  dziala tylko na MEGA2560

#include <SPI.h>
#include <MFRC522.h>
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

DFRobotDFPlayerMini myDFPlayer;

#define RST_PIN 5  /* Reset Pin */
#define SS_PIN 53  /* Slave Select Pin */




/* Use 'DumpInfo' example in MFRC522 Library to get RFID Card's UID */
/* Replace the following with your RFID UID */
/* Do not put 0's */
/* In my case, UID is F3 9E 3D 03 */
/* So, I put F39E3D3, without any 0's and spaces */ 
//String masterTagID1 = "FAE54F19";
//String masterTagID2 = "EA6554B4";
const char *masterTagID[22] = {"EC4FFD22", //1- BRELOK
                              "EA6554B4",  //2
                              "FAE54F19",  //3
                              "9AEB93AE",
                              "8A94FAD",
                              "BAA494AE",  //6
                              "9A604EAD",
                              "7A2094AE",
                              "4AB93BB3",  //9
                              "4AC850AD",
                              "7ABD7DAD",  //11
                              "FADF7FAD",
                              "9A844EAD",
                              "AE84EAD",  //14
                              "7AA13BB4",
                              "BAEB4EAD",  //16
                              "BABE7BAD",
                              "AF095AE",
                              "2ACC58B4",  //19
                              "CACF4FAD",
                              "8A745B3",
                              "6AE5ABAD" };  //22

/* Create a string to capture scanned card's UID */
String scanTagID = "";
String prevScanTagID = "";
int prevNr;

/* Create an instance of MFRC522 */
MFRC522 mfrc522(SS_PIN, RST_PIN);


#include <ClickEncoder.h>
#include <avdweb_Switch.h>
#include <TimerOne.h>

ClickEncoder *encoder;
int16_t last, value;
#define ENCODER_PINA 8  // 48
#define ENCODER_PINB 9  // 50
#define ENCODER_BTN 10

bool start=true;

boolean isPlaying = false;
int16_t volumeLevel = 0; //variable for holding volume level

#define ENCODER_STEPS_PER_NOTCH 4  // Change this depending on which encoder is used

Switch Next = Switch(2, INPUT_PULLUP, LOW, 50);
Switch Previous = Switch(3, INPUT_PULLUP, LOW, 50);
Switch Pause = Switch(4, INPUT_PULLUP, LOW, 50);

Switch Up = Switch(7, INPUT_PULLUP, LOW, 50);
Switch Down = Switch(6, INPUT_PULLUP, LOW, 50);

void setup() 
{
 Serial.begin(9600);
  Serial1.begin(9600);
  myDFPlayer.begin(Serial1);
  myDFPlayer.volume(2);
  SPI.begin();
  /* Initialize MFRC522 Module */
  mfrc522.PCD_Init();
  
  mfrc522.PCD_DumpVersionToSerial();	// Show details of PCD - MFRC522 Card Reader details
	Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
  
  encoder = new ClickEncoder(ENCODER_PINA, ENCODER_PINB, ENCODER_BTN, ENCODER_STEPS_PER_NOTCH);
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);
}

void timerIsr() {
  encoder->service();
}

void loop() {

  Next.poll();
  Previous.poll();
  Pause.poll();
  Up.poll();
  Down.poll();

  value += encoder->getValue();  // odczytaj aktualny stan czy encoder zmienil pozycje

  if (value != last) {
    
    if (last < value && volumeLevel < 30) {  //  jak przekrecono w prawo to zwieksz wartosc o 1
      volumeLevel++;
    }

    if (last > value && volumeLevel > 0) {  //  jak przekrecono w lewo to zmniejsz wartosc o 1
      volumeLevel--;
    }

    if (volumeLevel > 29) {
      volumeLevel = 30;
    }
    if (volumeLevel < 1) {
      volumeLevel = 0;
    }

    last = value;
    Serial.println(volumeLevel);
    myDFPlayer.volume(volumeLevel);
  }

  
  if(Up.pushed() && volumeLevel <= 30) {
    volumeLevel++;
    myDFPlayer.volume(volumeLevel);
    Serial.println("Up");
    Serial.println(volumeLevel);
  }

  if (Up.longPress()) {
    volumeLevel=30;
    myDFPlayer.volume(volumeLevel);
  }

  if(Down.pushed() && volumeLevel > 0) {
    volumeLevel--;
    myDFPlayer.volume(volumeLevel);
    Serial.println("Down");
    Serial.println(volumeLevel);
  }

  if (Down.longPress()) {
    volumeLevel=0;
    myDFPlayer.volume(volumeLevel);
  }
  //---------------------------------------------------
  if(Next.pushed()) {    
    myDFPlayer.next();
    Serial.println("Next");}
  //---------------------------------------------------
  if(Previous.pushed()) {    
    myDFPlayer.previous();
    Serial.println("Prev");
  }

  if(Pause.pushed()) {
    if(isPlaying == 1) {
      myDFPlayer.pause();
      isPlaying=0;
      Serial.println("PAUSE");       
      } else {
        myDFPlayer.start();    
        isPlaying=1;
        Serial.print("PLAY:");
        Serial.println(myDFPlayer.readCurrentFileNumber());
      }
  Serial.println("play pause");
  }

  while (readTagID()) {
    Serial.println(scanTagID);
    for (int i=0; i<4; i++) {
      if (scanTagID == masterTagID[i]) {         
        Serial.println("OKI");
        pause(i);        
      }      
    }    
  }
}

boolean readTagID() {
  if ( ! mfrc522.PICC_IsNewCardPresent())
  {
    return false;
  }
  if ( ! mfrc522.PICC_ReadCardSerial())
  {
    return false;
  }

  /* Clear the string */
  scanTagID = "";
  for ( uint8_t i = 0; i < 4; i++) {
    scanTagID += String(mfrc522.uid.uidByte[i], HEX);
  }

  scanTagID.toUpperCase();
  mfrc522.PICC_HaltA();
  return true;
}

void pause(int nr) {
  if (prevNr != nr){
    while(start){
      myDFPlayer.volume(2);
      Serial.println("START");
      start=false;
    }
    myDFPlayer.playFolder(15, nr+1);
    Serial.print("PLAY:");
    prevNr = nr;
  }

  if (isPlaying == 0 && prevNr == nr){      
    myDFPlayer.start();
    Serial.print("Kontynuuje:");         
    isPlaying=1;      
  } else{
    myDFPlayer.pause();
    Serial.print("PAUSE:");
    isPlaying=0;
  }    
  Serial.println(myDFPlayer.readCurrentFileNumber());  
}